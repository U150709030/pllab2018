use strict;
use warnings;

my $filename = $ARGV [0];

open IN, "<", $filename or die "Can not open the file: $!"; #read the file

while (<IN>) {
	chomp;                       #end of line, clear \n
	my @cols = split ('\t', $_); #return the columns - tab
	print "$cols[0] | $cols[1] | $cols[6]\n";
	
}

