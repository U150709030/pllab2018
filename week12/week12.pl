use strict;
use warnings;

hello();

my $avg = average(10,20,30);

print "Average of 10,20 and 30 is equal to $avg.\n";

print "Enter a DNA: ";
my $DNA = <>;
chomp ($DNA);
$DNA = myfunc($DNA);
print $DNA, "\n";

print "Enter a DNA:";
my $DNA2 = <>;
chomp ($DNA2);
myfunc2(\$DNA2);
print $DNA2, "\n";



sub hello {
	print "Hello World\n";
}

sub average {
	my $size = scalar @_;
	my $total = 0;
	
	for (my $i=0; $i<$size; $i++) {
		$total += $_[$i];
	}
	
	my $avg = $total / $size;
	return $avg;
}

sub myfunc {
	my ($DNA) = @_; #shift is another way: for first value,,,, = shift;
	if ($DNA =~ /atg/) {
		$DNA =~ s/atg/ATG/g; # s means subtitute, g means global
	}
	
	return $DNA; 
}
	
sub myfunc2 {
	my $DNAref = shift;
	
	if($$DNAref =~ /atg/) {  #~ means matching, first part is a pointer, pointer hash olsaydı %, second $ = value
		$$DNAref =~ s/atg/ATG/g;
	
	}
	
}

